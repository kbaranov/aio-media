<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m171129_155639_create_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'language_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'published_at' => $this->integer(),
            'title' => $this->string(),
            'text' => $this->text(),
            'likes' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-posts-language_id',
            'posts',
            'language_id'
        );

        $this->createIndex(
            'idx-posts-author_id',
            'posts',
            'author_id'
        );

        $this->addForeignKey(
            'fk-posts-language_id',
            'posts',
            'language_id',
            'languages',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-posts-author_id',
            'posts',
            'author_id',
            'authors',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('posts');
    }
}
