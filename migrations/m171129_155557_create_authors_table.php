<?php

use yii\db\Migration;

/**
 * Handles the creation of table `authors`.
 */
class m171129_155557_create_authors_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->batchInsert('authors', ['name'], (function(){
            $seeds = ["CrazyNews", "Чук и Гек", "CatFuns", "CarDriver", "BestPics", "ЗОЖ", "Вася Пупкин",
                "Готовим со вкусом", "Шахтёрская Правда", "FunScience"];
            $rows = [];
            foreach ($seeds as $item) {
                $rows[] = [$item];
            }
            return $rows;
        })());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('authors');
    }
}
