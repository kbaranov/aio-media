<?php

namespace app\commands;

use yii\console\Controller;
use Yii;

/**
 * This command creates seeds into database.
 *
 */
class SeedsController extends Controller
{
    /**
     * This command generates N posts.
     *
     * @param int $quantity
     * @return int
     */
    public function actionPosts($quantity = 10)
    {
        $date_from = '01.01.2017';
        $date_to = '08.08.2017';
        $lang_mapper = ['', 'ru', 'en'];
        $languages = Yii::$app->db->createCommand("SELECT id FROM languages")->queryColumn();
        $authors = Yii::$app->db->createCommand("SELECT id FROM authors")->queryColumn();
        $rows = [];
        while ($quantity > 0) {
            $language_id = $languages[array_rand($languages)];
            $rows[] = [
                $language_id,
                $authors[array_rand($authors)],
                rand(strtotime($date_from), strtotime($date_to)),
                $this->generateTitle($lang_mapper[$language_id]),
                $this->generateText($lang_mapper[$language_id]),
                $this->generateLikes(),
            ];
            $quantity--;
        }
        Yii::$app->db->createCommand()->batchInsert('posts', [
            'language_id', 'author_id', 'published_at', 'title', 'text', 'likes',
        ], $rows)->execute();

        return 0;
    }

    /**
     * Возвращает заголовок из нескольких слов
     *
     * @param string $language
     * @return string
     */
    private function generateTitle($language = 'en')
    {
        $seeds = Yii::$app->params['seeds']['posts']['titles'][$language];

        return $this->generateSentence($seeds,4, 6);
    }

    /**
     * Возвращает текст из нескольких предложений
     *
     * @param string $language
     * @return string
     */
    private function generateText($language = 'en')
    {
        $seeds = Yii::$app->params['seeds']['posts']['texts'][$language];

        $sentences_count = rand(3, 4);
        $sentences = [];
        while ($sentences_count > 0) {

            $sentences[] = $this->generateSentence($seeds,5, 8) . '.';

            $sentences_count--;
        }

        return implode(' ', $sentences);
    }

    /**
     * Возвращает предложение
     *
     * @param array $words
     * @param int $size_min
     * @param int $size_max
     * @return string
     */
    private function generateSentence($words, $size_min, $size_max)
    {
        $words_count = rand($size_min, $size_max);
        $word_list = [];
        while ($words_count > 0) {
            $word_list[] = $words[rand(0, count($words) - 1)];
            $words_count--;
        }

        $word_list[0] = mb_convert_case($word_list[0], MB_CASE_TITLE);

        return implode(' ', $word_list);
    }

    /**
     * Возвращает случайное количество лайков с неравномерным распределением
     *
     * @return int
     */
    private function generateLikes()
    {
        return rand(0, rand(0, 100));
    }
}
