<?php

return [
    'adminEmail' => 'admin@example.com',
    'seeds' => require __DIR__ . '/seeds.php',
];
