<?php

use yii\db\Migration;

/**
 * Handles the creation of table `languages`.
 */
class m171129_155413_create_languages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('languages', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->batchInsert('languages', ['name'], (function(){
            $seeds = ["Русский", "English"];
            $rows = [];
            foreach ($seeds as $item) {
                $rows[] = [$item];
            }
            return $rows;
        })());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('languages');
    }
}
